module.exports = {
    queues: {
        EXTRACT_VIDEO_BACKGROUND_MUSIC_QUEUE: 'EXTRACT_VIDEO_BACKGROUND_MUSIC_QUEUE',
        EXTRACT_VIDEO_BACKGROUND_MUSIC_FINISH_QUEUE: 'EXTRACT_VIDEO_BACKGROUND_MUSIC_FINISH_QUEUE',
    }
}