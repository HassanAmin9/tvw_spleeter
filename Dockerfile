FROM hassanamin994/node_ffmpeg_spleeter:1

WORKDIR /tvw_spleeter

COPY package*.json ./
RUN npm install

COPY . .
RUN echo "GOOGLE_APPLICATION_CREDENTIALS=/tvw_spleeter/gsc_creds.json" >> .env

# ADD AWS CREDENTIALS FILE
ARG AWS_KEYS_FILE_BASE64
RUN mkdir ~/.aws
RUN echo ${AWS_KEYS_FILE_BASE64} | base64 --decode > ~/.aws/credentials

CMD ["npm", "run", "docker:prod"]