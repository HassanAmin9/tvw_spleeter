const fs = require('fs');
const async = require('async');
const uuid = require('uuid').v4;
const path = require('path');
const queues = require('../constants').queues;

const videoHandler = require('../dbHandlers/video');
const articleHandler = require('../dbHandlers/article');

const utils = require('../utils');
const converter = require('../converter');
const storageService = require('../vendors/storage');

const onExtractVideoBackgroundMusic = channel => (msg) => {
    const { videoId } = JSON.parse(msg.content.toString());
    let video;
    let article;
    let videoPath;
    // download original video
    // cut it using the timing provided by the user
    // cut silent parts and add them as slides
    // uploaded cutted parts
    // cleanup
    const tmpDir = path.join(__dirname, '../tmp', `background-music-dir-${uuid()}`);
    fs.mkdirSync(tmpDir)
    console.log('got request to extract', videoId);

    videoHandler.findById(videoId)
        .then(v => {
            if (!v) throw new Error('Invalid video id');
            console.log('Extracting background music for', v)
            video = v;
            videoPath = path.join(tmpDir, `${uuid()}.${utils.getFileExtension(video.url)}`);
            return utils.downloadFile(video.url, videoPath);
        })
        // extract background music
        .then((videoPath) => {
            console.log('started extracting')
            const backgroundMusicPath = tmpDir;
            return converter.extractBackgroundMusic(videoPath, backgroundMusicPath)
        })
        // Compress file
        .then((backgroundMusicPath) => {
            const compressedBackgroundMusicPath = path.join(tmpDir, `compressed-bg.mp3`)
            return converter.compressAudioFile(backgroundMusicPath, compressedBackgroundMusicPath)
        })
        .then((backgroundMusicPath) => {
            console.log('uploading file')
            return storageService.saveFile('backgroundMusic', `${uuid()}-${backgroundMusicPath.split('/').pop()}`, fs.createReadStream(backgroundMusicPath))
        })
        .then((uploadRes) => {
            console.log('upload res', uploadRes);
            return videoHandler.updateById(videoId, { backgroundMusicUrl: uploadRes.url, backgroundMusicKey: uploadRes.data.Key, extractBackgroundMusicLoading: false })
        })
        .then(() => {
            console.log('done');
            utils.cleanupDir(tmpDir);
            channel.sendToQueue(queues.EXTRACT_VIDEO_BACKGROUND_MUSIC_FINISH_QUEUE, new Buffer(JSON.stringify({ videoId })), { persistent: true });
            channel.ack(msg);
        })
        .catch(err => {
            console.log(err);
            
            utils.cleanupDir(tmpDir);
            channel.ack(msg);
            videoHandler.updateById(videoId, { extractBackgroundMusicLoading: false })
            .then(() => {
                channel.sendToQueue(queues.EXTRACT_VIDEO_BACKGROUND_MUSIC_FINISH_QUEUE, new Buffer(JSON.stringify({ videoId })), { persistent: true });
            })
        })
}


module.exports = onExtractVideoBackgroundMusic;